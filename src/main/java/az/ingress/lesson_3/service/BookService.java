package az.ingress.lesson_3.service;

import az.ingress.lesson_3.dto.BookRequest;
import az.ingress.lesson_3.dto.BookResponse;
import org.springframework.stereotype.Service;

import java.util.List;


public interface BookService {
    int createBook(BookRequest request);

    BookResponse updateBook(Integer id, BookRequest request);

    BookResponse getBookById(Integer id);

    List<BookResponse> getAllBooks();

    void deleteBookById(Integer id);
}
